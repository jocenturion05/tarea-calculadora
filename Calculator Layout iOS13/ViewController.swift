//
//  ViewController.swift
//  Calculator Layout iOS13
//
//  Created by Angela Yu on 01/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//
import UIKit
var cantNum = [String]()
var StringRep = "0"
var aux: String!
var num: String!
class ViewController: UIViewController {
    @IBOutlet weak var labelResult: UILabel!
    @IBOutlet weak var buttonPerc: UIButton!
    @IBOutlet weak var buttonConv: UIButton!
    @IBOutlet weak var buttonAC: UIButton!
    @IBOutlet weak var buttonDiv: UIButton!
    @IBOutlet weak var buttonSeven: UIButton!
    @IBOutlet weak var buttonEight: UIButton!
    @IBOutlet weak var buttonNine: UIButton!
    var num_1 = 0
    var num_2 = 0
    var numAgg_1 = "0"
    var numAgg_2 = "0"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBAction func buttonNum(_ sender: UIButton) {
        num = sender.title(for: .normal)!
        cantNum.append(num)
        StringRep = cantNum.joined(separator: "")
        labelResult.text = StringRep
    }
    @IBAction func butttonOperate(_ sender: UIButton) {
        if(sender.title(for: .normal) == "AC"){
            cantNum.removeAll()
            num_1 = 0
            numAgg_1 = "0"
            StringRep = "0"
            labelResult.text = "0"
        }
        if(sender.title(for: .normal) == "+" || sender.title(for: .normal) == "x" || sender.title(for: .normal) == "÷" || sender.title(for: .normal) == "-" || sender.title(for: .normal) == "+/-" || sender.title(for: .normal) == "%" ){
            labelResult.text = StringRep
            aux = sender.title(for: .normal)
            if(StringRep != "0"){
                numAgg_1 = StringRep
                let myIntegerVariable = Int(numAgg_1) ?? 0
                num_1 = myIntegerVariable
                cantNum.removeAll()
            }
            if(sender.title(for: .normal) == "+/-"){
                let myIntegerVariable = Int(StringRep) ?? 0
                let total = -num_1
                let totalS = String(total)
                labelResult.text = totalS
                aux = "0"
                cantNum.removeAll()
            }
        }
        if( sender.title(for: .normal) == "="){
            if(aux == "+"){
                let myIntegerVariable = Int(StringRep) ?? 0
                let total = num_1 + myIntegerVariable
                let totalS = String(total)
                labelResult.text = totalS
                aux = "0"
                cantNum.removeAll()
            }
            if(aux == "x"){
                let myIntegerVariable = Int(StringRep) ?? 0
                let total = num_1 * myIntegerVariable
                let totalS = String(total)
                labelResult.text = totalS
                aux = "0"
                cantNum.removeAll()
            }
            if(aux == "÷"){
                let myIntegerVariable = Int(StringRep) ?? 0
                let total = num_1 / myIntegerVariable
                let totalS = String(total)
                labelResult.text = totalS
                aux = "0"
                cantNum.removeAll()
            }
            if(aux == "-"){
                let myIntegerVariable = Int(StringRep) ?? 0
                let total = num_1 - myIntegerVariable
                let totalS = String(total)
                labelResult.text = totalS
                aux = "0"
                cantNum.removeAll()
            }
            if(aux == "%"){
                let myIntegerVariable = Int(StringRep) ?? 0
                let total = num_1 * myIntegerVariable/100
                let totalS = String(total)
                labelResult.text = totalS
                aux = "0"
                cantNum.removeAll()
            }
        }
    }
}

